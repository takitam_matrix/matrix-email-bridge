# Matrix-email Bridge

This repo contains (almost) transparent matrix gateway.
Following features are implemented:
*  Sending and receiving emails, both plaintext and HTML.  
   Bridge will dynamically create rooms and invite matrix users/mail puppets when new message appears, see 'Usage' section.
*  Turning matrix room into *something like* mailing list.
*  Sending and receiving attachments (looks ugly, see end of file).

### Architecture overview

      +-----------+                                        +-----------+
      |           |             +---------------+          |           |
      |           >===AS=API====>               >===SMTP===>           |
      | Matrix HS |             |  Matrix-email |          |   Email   |
      |           <===Client/===<     Bridge    <===LMTP===<   Server  |
      |           |   AS API    |               |          |           |
      |           |             +-------A-------+          |           |
      |           |                     |                  |           |
      +-----------+                     |                  +-----------+
                                   +----V----+
                                   |Bridge DB|
                                   |  (SQL)  |
                                   +---------+

### This project is currently in Beta state (and uses libs that are also in Beta to make life funny), some important features are still missing, see end of this file.

## Usage

### Dynamic (user) bridging

Invite bot and send command `!mb register`, bridge will generate email address for you.  
From this moment people can send you emails and you will receive matrix messages.  
If you want to start conversation yourself send command `!mb msg number1@email.com number2@another.email.net`.

Keep in mind that bridge assigns room to the set of message receivers,  
so if email user anon@random.de will send a message to your bridged email new room will be created to handle messages between you and anon@random.de,  
if anon@random.de will send a message to you and another@dude.jp new room will be created for messages that are sent between you, another@dude.jp and anon@random.de.  

### Plumbed rooms

You can also use this software to bridge rooms. Just invite bot to room and send command `!mb bridge`.  
Bridge will generate email address for room, when someone will send message to it, mail puppet will join room and send message.  
When someone (including mail puppets) sends message in bridged room puppets that participate in room will get emails;  
Bridge will set room's mail address in reply-path and 'From' header but with sender's matrix display name in email display name.  

Remember that (by default) you need to have power lvl 90 to bridge room and room needs to have canonical alias (main address).  
Additionally, at this moment, you need to allow anyone to join room, because otherwise mail puppets will not be able to join.

### Installation

See `INSTALL.md` for installation instructions.

## Big TODOs
*  Due to bridge access control implemented in used libs,  
   even when bridge is used to room only messages from whitelisted users will be sent (assuming that you limit bridge usage to your domain).
*  There is no support for message subject, cc and possibly other things.
*  It's not possible to unbridge room/remove user.
*  It's not possible to invite puppet to room, so new email users can not join invite only rooms in any way.
*  Bridge access control appears to be broken, it's possible to add only one token in `bridge.permissions`.
*  Configuration file can be overwritten by default values from `example-config.yaml`.
*  It's possible to enumerate users as bridge will return `554 no valid recipients` if new email would have any matrix recipient.
*  Bridge is not hardened against mail loops, some research needs to be done here.
#### LMPT is not fully implemented and under some rare conditions bridge may **silently** fail to deliver message.
#### If user leaves dynamically created room, it's not possible to go back.

## Things that does not work well, but would require lot of work
*  This bridge sends one mail on one matrix event, it means that when matrix user send file empty email with attachment will be sent.
