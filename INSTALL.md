## Installation

As usual, make backups and do not do dangerous changes when server is under heavy load.  
If you have bad luck, synapse/MTA may stop work, so be ready to undo changes at any moment.

### Mail server configuration

This bridge uses LMTP protocol and heavily depends on a 'real' SMTP server to handle message queuing, TLS, spam filtering and many other things.  
In addition to the bridge configuration itself, you need to prepare an email server.  

To avoid name collision between 'normal' email users and matrix users you need to get at least one additional MX record.  
You also should add another one to avoid collision between rooms and users but you can tell bridge to use localpart prefixes,  
so instead addresses like user1@matrix.domain.my and room1@room.domain.my you will get u\_user1@bridge.domain.my and r\_room1@bridge.domain.my,  
see key 'user\_mail\_template' in example config.  

Once DNS is configured you need to tell your mail server about your matrix gateway and redirect selected domains to it.  

On exim you need to add router  

```
matrix_gateway_router:
    driver = accept
    transport = matrix_gateway
    domains = matrix.domain.my:room.domain.my
```

and transport  

```
matrix_gateway:
    driver = smtp
    protocol = lmtp
    hosts = localhost
    allow_localhost = true
    port = 8025
```

### Bridge Setup

Default configuration file and systemD service assume that you have bridge on same machine as HS.  

1.  Create directory `/opt/matrix/matrix-lmtp`. We are going to use it as root directory for bridge.  
    If you want to use different path you need to adjust included systemD service.
2.  Add new user for bridge `useradd --system --shell /usr/sbin/nologin matrix-lmtp`, included systemD service will drop privileges to it.
3.  Create directory `data/` in bridge root directory. Change owner of `data/` directory to `matrix-lmtp`.  
	With default configuration bridge will store there logs and database.
4.  Copy `matrix-lmtp.service` to `/etc/systemd/system`.
5.  You may want to drop privileges now as we are going to use pip now. If you are going to use unprivileged user, remember to adjust directory permissions.  
    Run `python3.7 -m venv ./venv` to create python virtual environment, then activate it with `. venv/bin/activate`.  
	Run `pip install git+https://gitlab.com/takitam_matrix/matrix-email-bridge.git`
6.  Download and adjust configuration file; you need at least to change domains, you also may want to change used ports and email address templates.  
    Upload configuration file as `config.yaml` to bridge root directory,  
    then run `ln -s config.yaml example-config.yaml && chown matrix-lmtp config.yaml`. This is workaround for bug in used libs.  
    If you allow only users from your domain to use bridge, you may remove `_{hs_domain}` from `*mail_template` options.  
    Remember that config file contain appservice keys, so it must not be world readable!
7.  Generate registration file;  
    Run `sudo -u matrix-lmtp ./venv/bin/python -m matrix_email_bridge --generate-registration -r ./data/matrix-lmtp.yaml && chmod o-r ./data/matrix-lmtp.yaml`  
    Copy registration file to synapse configuration dir. You also need to add this file to synapse config (section `app_service_config_files:`).  
    Remember to adjust permissions so synapse can read this file.  
8.  Run `sudo -u matrix-lmtp bash` and `cd /opt/matrix/matrix-lmtp/data`.  
    Run `alembic init alembic` and follow instructions.  
	Configure alembic; Probably the only thing that you want to do is setting `sqlalchemy.url = sqlite:///lmtp-bridge.db` in `alembic.ini`.  
    Finally, run `alembic upgrade head`.
9.  Switch back to root and run `systemctl enable matrix-lmtp` and restart synapse, it should also start bridge.  
    Now pray and run `watch 'systemctl status -l matrix-synapse ; systemctl status -l matrix-lmtp'`, monitor it for ~20 seconds;  
    synapse can start and die after few seconds, if registration file is malformed... BUT Installation should be done now :)  
