import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="matrix-email-bridge-takitam",
    version="0.9",
    author="takitam",
    author_email="takitam.matrix@protonmail.com",
    description="Dynamic matrix-email bridge",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/takitam_matrix/matrix-email-bridge",
    packages=setuptools.find_packages(),
    install_requires=[
        "aiosmtpd",
        "sqlalchemy",
        "alembic",
        "ruamel.yaml",
        "commonmark",
        "mautrix==0.4.2"
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)"
        "Topic :: Communications :: Chat",
        "Topic :: Communications :: Email",
        "Framework :: AsyncIO",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
    ],
    python_requires='>=3.7',
)
