#!/usr/bin/python3.7

import copy
from itertools import repeat
import hashlib
import asyncio
import concurrent
import re
from abc import ABC
from typing import Dict, Any
from smtplib import SMTP
import email
from email import policy
from email.message import EmailMessage
from email.header import Header
from email.utils import formataddr
from aiosmtpd.lmtp import LMTP
from aiosmtpd.handlers import Message
import mautrix.bridge.db
from mautrix.bridge import Bridge, BaseMatrixHandler, BaseBridgeConfig, BaseUser, BasePortal, BasePuppet
from mautrix.bridge.commands import command_handler
from mautrix.errors import MNotFound, MatrixError
from mautrix.types import UserID, RoomID, MessageEventContent, TextMessageEventContent, MediaMessageEventContent, EventID, Format, EventType
import mautrix.util.db as bdb
from . import db_tables

MAIL_RE = re.compile(r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~\-]+@[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*$')


class MatrixLmtpBridgeError(MatrixError, ABC):
    pass

# TODO matrix-side error reporting!
class TeporaryBridgeError(MatrixLmtpBridgeError):
    def __init__(self, reply='451 Requested action aborted: local error in processing', \
                 times=1):
        self.reply = '\r\n'.join(repeat(reply, times))

class PernamentBridgeError(MatrixLmtpBridgeError):
    def __init__(self, reply='554 Transaction failed', times=1):
        self.reply = '\r\n'.join(repeat(reply, times))

class NoSuchPortalError(MatrixLmtpBridgeError):
    pass


class Config(BaseBridgeConfig):
    @property
    def namespaces(self):
        return {
            'users': self['appservice.namespace.users'],
            'aliases': self['appservice.namespace.aliases']
        }


class LmtpHandler(Message):
    async def handle_DATA(self, server, session, envelope):
        env = copy.deepcopy(envelope)
        try:
            reply = await LmtpPortal.handle_transaction(env)
        except MatrixLmtpBridgeError as e:
            return e.reply
        except Exception as e: # aiosmtpd is going to catch it anyway
            self.log.error(e, exc_info=True)
            return '\r\n'.join(repeat('500 Unknown error', len(envelope.rcpt_tos)))
        return reply


def factory():
    return LMTP(LmtpHandler())


class LmtpUser(BaseUser):
    is_whitelisted: bool
    mxid: UserID
    mail: str
    usertype: db_tables.UserType
    command_status: Dict[str, Any]

    def __init__(self, mxid, mail, usertype):
        self.mail = mail
        self.mxid = mxid
        self.usertype = usertype

    @property
    def is_whitelisted(self):
        return LmtpBridge.test_perms(self.mxid, 'user')

    @classmethod
    def get_by_mail(cls, mail) -> 'LmtpUser':
        result = db_tables.Users.queryByMail(mail)

        if result and result.usertype == db_tables.UserType.real:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype))
        else:
            return None

    @classmethod
    def get_by_mxid(cls, mxid: UserID) -> 'LmtpUser':
        result = db_tables.Users.queryByMxid(mxid)
        if result and result.usertype == db_tables.UserType.real:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype))
        else:
            return cls(mxid, None, None)

    async def is_logged_in(self) -> bool:
        return True

    @classmethod
    def userMxid2email(cls, mxid):
        local_part = mxid.split(':')[0][1:]
        hs_domain = mxid.split(':')[1]
        mailLocalPart = \
            cls.config["bridge.user_mail_template"].format(localpart=local_part, \
                                                           hs_domain=hs_domain)
        domain = cls.config["bridge.user_domain"]
        return f"{mailLocalPart}@{domain}"


class LmtpPuppet(BasePuppet):
    is_whitelisted: bool
    mxid: UserID
    mail: str
    usertype: db_tables.UserType
    command_status: Dict[str, Any]

    def __init__(self, mxid, mail, usertype, is_whitelisted):
        self.mail = mail
        self.default_mxid = mxid
        self.usertype = usertype
        self.is_whitelisted = is_whitelisted

        self.custom_mxid = None

    @classmethod
    def get_by_mail(cls, mail) -> 'LmtpPuppet':
        result = db_tables.Users.queryByMail(mail)
        if not result:
            mxid = cls.insertUser(mail) # TODO do not create puppets in domain dedicated for users/rooms
        elif not result.usertype == db_tables.UserType.mailpuppet:
            return None
        else:
            mxid = result.mxid
        return cls(mxid, mail, db_tables.UserType.mailpuppet, True)

    @classmethod
    def get_by_mxid(cls, mxid: UserID) -> 'LmtpPuppet':
        result = db_tables.Users.queryByMxid(mxid)
        if result and result.usertype == db_tables.UserType.mailpuppet:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype), True)
        else:
            return None

    def save(self) -> None:
        pass

    @classmethod
    def insertUser(cls, mail):
        mxid = cls._mail2mxid(mail)
        db_tables.Users.insert(mail, mxid, db_tables.UserType.mailpuppet)
        asyncio.ensure_future(cls.az.intent.user(mxid).set_displayname(mail))
        return mxid

    ## It would be faster and easier to just use email address
    #  as mxid but it would not be possible for some email addresses.
    #  see https://stackoverflow.com/q/2049502
    #  and https://matrix.org/docs/spec/appendices#user-identifiers
    @classmethod
    def _mail2mxid(cls, mail):
        raw = mail.encode()
        hexdig = hashlib.sha256(raw).hexdigest()
        local_part = cls.config["bridge.username_template"].format(userid=hexdig)
        domain = cls.config["homeserver.domain"]
        return f"@{local_part}:{domain}"

    async def is_logged_in(self) -> bool:
        return True


class LmtpReplies:
    def __init__(self, tos_list):
        self.replies = []
        for addr in tos_list:
            self.replies.append([addr, None])

    def get_replies(self):
        out = []
        for addr_reply in self.replies:
            reply = addr_reply[1]
            if reply is None:
                raise ValueError(f'Reply for addr {addr_reply[0]} is not set')
            out.append(reply)
        return '\r\n'.join(out)

    # TODO if tos_list is guaranted to be uniqe then regular dict can replace it
    def __setitem__(self, key, value):
        something_replaced = False
        for addr_reply in self.replies:
            if addr_reply[0] == key:
                something_replaced = True
                addr_reply[1] = value
        if not something_replaced:
            raise ValueError(f'Unable to find "{key}" in reply list.')


class LmtpPortal(BasePortal):
    def __init__(self, room_id):
        self.room_id = room_id
        if db_tables.Rooms.have(room_id, db_tables.PortalType.dynamic):
            self._type = db_tables.PortalType.dynamic
        elif db_tables.Rooms.have(room_id, db_tables.PortalType.plumbed):
            self._type = db_tables.PortalType.plumbed
        else:
            return None

    @classmethod
    async def handle_transaction(cls, envelope):
        cls.log.debug(f'Handling email from {envelope.mail_from} to {envelope.rcpt_tos}')
        replies = LmtpReplies(envelope.rcpt_tos)
        user_tos = []
        for addr in envelope.rcpt_tos:
            plumbed_id = db_tables.Rooms.queryByAddr(addr, db_tables.PortalType.plumbed)
            if plumbed_id:
                portal = LmtpPortal(plumbed_id)
                reply = '250 OK'
                try:
                    await portal.handle_email_message(envelope)
                except MatrixError:
                    cls.log.error(\
                    "Matrix error in email handler while processing message for {addr}",
                    exc_info=True)
                    reply = f'500 <{addr}> Unknown error on matrix side'
                except:
                    cls.log.error(\
                    "Unexpected error while processing message for {addr}. Aborting", exc_info=True)
                    raise
                replies[addr] = reply
            else:
                user_tos.append(addr)

        if user_tos:
            try:
                portal = await LmtpPortal.get_by_mail_members(user_tos + [envelope.mail_from])
                await portal.handle_email_message(envelope)
                for addr in user_tos:
                    replies[addr] = '250 OK'
            except MatrixError:
                for addr in user_tos:
                    replies[addr] = '500 Unknown error'
            except:
                cls.log.error("Unexpected error while processing message for {addr}. Aborting",
                              exc_info=True)
                raise
        return replies.get_replies()

    @classmethod
    async def get_by_mail_members(cls, members_addr):
        cls.log.debug(f'Getting dynamic portal for {members_addr} ...')
        addrhash = cls._rcpt2Hash(members_addr)
        room_id = db_tables.Rooms.queryByAddr(addrhash, db_tables.PortalType.dynamic)

        if room_id is None:
            room_id = await cls.create_portal_by_mail_members(members_addr)

        cls.log.debug(f'Got portal with id {room_id} for {members_addr}')
        return cls(room_id)

    @classmethod
    async def create_portal_by_mail_members(cls, members_addr):
        all_users = []
        puppets = []
        addrhash = cls._rcpt2Hash(members_addr)
        cls.log.debug(f'Creating portal for {members_addr} hash:{addrhash}')

        for memberAddr in members_addr:
            usr = LmtpUser.get_by_mail(memberAddr)
            if usr:
                try:
                    all_users.append(usr.mxid)
                except AttributeError:
                    cls.log.warning(f"Failed to get user for {memberAddr}")
            else:
                puppet = LmtpPuppet.get_by_mail(memberAddr)
                if puppet.mxid:
                    puppets.append(puppet.mxid)
                    all_users.append(puppet.mxid)
                else:
                    cls.log.warning(f"Failed to get puppet for {memberAddr}")

        if all_users:
            room_id = \
                await cls.az.intent.create_room(name=' '.join(members_addr),
                                                invitees=all_users)
            cls.log.debug(f'Created portal with roomid {room_id}')
            db_tables.Rooms.insert(room_id, addrhash, db_tables.PortalType.dynamic)
            cls.log.debug(f'Joining {puppets} to {room_id}')
            for puppet_id in puppets:
                await cls.az.intent.user(puppet_id).join_room(room_id)
            return room_id
        else:
            cls.log.warning(f"Not creating empty room for {members_addr}")
            raise PernamentBridgeError(reply='503 no valid recipients', times=1)

    async def handle_email_message(self, envelope) -> None:
        ## Remember: If you are going to use envelope.rcpt_tos here,
        #  read method handle_transaction first
        # TODO invite all users again
        sender = LmtpPuppet.get_by_mail(envelope.mail_from)
        message = email.message_from_bytes(envelope.content, policy=policy.default)

        mimehtmlbody = message.get_body(preferencelist=('html'))
        mimeplainbody = message.get_body(preferencelist=('plain'))

        plainbody = htmlbody = None

        if mimeplainbody:
            plainbody = mimeplainbody.get_content()
        if mimehtmlbody:
            htmlbody = mimehtmlbody.get_content()

        self.log.debug(f"Sending matrix message from {envelope.mail_from} to room {self.room_id}")
        await self.az.intent.user(sender.mxid).send_text(self.room_id, text=plainbody,
                                                         html=htmlbody)

        for attachment in message.iter_attachments():
            atname = attachment.get_filename()
            atmime = attachment.get_content_type()
            atbytes = attachment.get_content()

            aturl = await self.az.intent.user(sender.mxid).upload_media(data=atbytes,
                                                                        mime_type=atmime,
                                                                        filename=atname)
            await self.az.intent.user(sender.mxid).send_file(self.room_id, url=aturl,
                                                             file_name=atname)

    async def handle_matrix_message(self, sender: 'BaseUser',
                                    message: MessageEventContent,
                                    event_id: EventID) -> None:
        puppets = await self.get_puppets()

        if not hasattr(self, '_type') or self._type == db_tables.PortalType.dynamic and \
               sender.usertype == db_tables.UserType.mailpuppet:
            self.log.warning(f"Dont know how to handle {event_id} in room {self.room_id}")
            return

        # TODO
        sender_or_puppet = LmtpPuppet.get_by_mxid(sender.mxid) or sender

        puppet_mail_addrs = []
        for puppet in puppets:
            assert puppet.mail
            if puppet.mxid == sender_or_puppet.mxid:
                continue
            puppet_mail_addrs.append(puppet.mail)

        if self._type == db_tables.PortalType.dynamic:
            if not sender_or_puppet.mail:
                self.log.warning(f"Sender {sender_or_puppet.mxid} miss email addr. Ignoring {event_id}")
                return # It should not be possible...
            header_from = return_path = sender_or_puppet.mail
        else: # plumbed
            return_path = db_tables.Rooms.queryByRoomId(self.room_id, \
                                                        db_tables.PortalType.plumbed)
            display_name = await self.az.intent.get_displayname(sender_or_puppet.mxid)
            header_from = formataddr((str(Header(display_name, 'utf-8')), return_path))

        if not puppet_mail_addrs:
            self.log.warning(f"There is no valid mail puppet in room {self.room_id}")
            return

        mail_msg = EmailMessage()

        mail_msg['Subject'] = 'Bridged Matrix message' # TODO allow setting Subject with specially formatted msg
        mail_msg['To'] = puppet_mail_addrs
        mail_msg['From'] = header_from

        if type(message) == TextMessageEventContent:
            mail_msg.set_content(message.body)
            if message.formatted_body:
                if message.format == Format.HTML:
                    mail_msg.add_alternative(message.formatted_body, subtype='html')
                else:
                    self.log.warning(f"Message with id {event_id} in room {self.room_id} has unknown format.")
        elif type(message) == MediaMessageEventContent:
            try:
                maintype, subtype = message.info.mimetype.split('/', 1)
            except AttributeError:
                maintype = 'application'
                subtype = 'octet-stream'
            data = await self.az.intent.user(sender_or_puppet.mxid).download_media(message.url)
            mail_msg.add_attachment(data, maintype=maintype, subtype=subtype,
                                    filename=message.body)
        else:
            self.log.warning(f"Message with id {event_id} in room {self.room_id} has unknown type.")
            return

        self.log.debug(f"Sending email from {return_path} to {mail_msg['To']} based on {event_id}.")
        with concurrent.futures.ThreadPoolExecutor() as executor:
            loop = asyncio.get_event_loop()
            await loop.run_in_executor(executor, self._sendmail, return_path, mail_msg)

    def _sendmail(self, return_path, message):
        with SMTP(self.config["mta.hostname"], self.config["mta.port"], \
                  self.config["appservice.id"]) as smtp:
            smtp.sendmail(return_path, message['To'], message.as_bytes())

    async def get_puppets(self):
        member_ids = await self.az.intent.get_room_members(self.room_id)
        puppets = []
        for mxid in member_ids:
            puppet = LmtpPuppet.get_by_mxid(mxid)
            if puppet:
                puppets.append(puppet)
        return puppets

    @staticmethod
    def _rcpt2Hash(rcptList):
        sortedAddr = sorted(set(rcptList))
        raw = '\0'.join(sortedAddr).encode()
        return hashlib.sha256(raw).hexdigest()

    @classmethod
    def alias2mail(cls, alias):
        localpart = alias.split(':')[0][1:]
        if not re.match(r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~\-]', alias) or \
               re.match(r'.*\.\..*', alias):
            raise ValueError

        domain = cls.config["bridge.room_domain"]
        hs_domain = alias.split(':')[1]
        mail_localpart = \
            cls.config["bridge.room_mail_template"].format(localpart=localpart, \
                                                           hs_domain=hs_domain)
        return f'{mail_localpart}@{domain}'


class MatrixHandler(BaseMatrixHandler):
    async def get_user(self, user_id: UserID) -> 'BaseUser':
        return LmtpUser.get_by_mxid(user_id)

    async def get_portal(self, room_id: RoomID) -> 'BasePortal':
        return LmtpPortal(room_id)

    async def get_puppet(self, user_id: UserID) -> 'BasePuppet':
        return LmtpPuppet.get_by_mxid(user_id)


@command_handler()
async def msg(evt):
    mails = copy.deepcopy(evt.args)
    for mail in mails:
        if not MAIL_RE.match(mail):
            await evt.reply(f'ERROR: Mail "{mail}" is not valid.')
            return
    if not hasattr(evt.sender, 'mail') or type(evt.sender.mail) != str:
        await evt.reply(f'ERROR: Your matrix account have no email address.')
        return
    mails.append(evt.sender.mail)
    try:
        await LmtpPortal.get_by_mail_members(mails)
    except MatrixLmtpBridgeError:
        await evt.reply(f'ERROR: Failed to create room.')
        return

@command_handler()
async def register(evt):
    if evt.args:
        await evt.reply(f'This command does not take any arguments.')
        return

    mxid = evt.sender.mxid
    exiting_mail = db_tables.Users.get_mail(mxid)

    if exiting_mail:
        await evt.reply(f'You already have email: {exiting_mail}')
        return

    mail = LmtpUser.userMxid2email(mxid)
    db_tables.Users.insert(mail, mxid, db_tables.UserType.real)
    await evt.reply(f'Your bridged email address is: {mail}')

@command_handler()
async def bridge(evt):
    if evt.args:
        await evt.reply('This command does not take any arguments.')
        return

    lvls = await evt.az.intent.get_power_levels(evt.room_id, ignore_cache=True) # TODO cache is broken
    if lvls.get_user_level(evt.sender.mxid) < evt.config['bridge.room_bridge_power_lvl']:
        await evt.reply('ERROR: You have too small power level to bridge this room.')
        return

    async def missing_alias():
        await evt.reply('ERROR: This room does not have canonical alias (main address).')

    try:
        alias_event = await evt.az.intent.get_state_event(evt.room_id, \
                                                          EventType.ROOM_CANONICAL_ALIAS)
    except MNotFound:
        await missing_alias()
        return
    if not alias_event or not hasattr(alias_event, 'canonical_alias') or \
       not alias_event.canonical_alias:
        await missing_alias()
        return

    try:
        addr = LmtpPortal.alias2mail(alias_event.canonical_alias)
    except ValueError:
        await evt.reply('ERROR: This room have invalid canonical alias (main address).')
        return

    existing_addr = db_tables.Rooms.queryByRoomId(evt.room_id, \
                                                  db_tables.PortalType.plumbed)
    if existing_addr:
        await evt.reply(f'This room already have email address: {existing_addr}')
        return

    db_tables.Rooms.insert(evt.room_id, addr, db_tables.PortalType.plumbed)
    await evt.reply(f'Bridged email address for this room is: {addr}')


class LmtpBridge(Bridge):
    name = 'EmailBridge'
    description = 'test'
    command = 'placeholder'
    config_class = Config
    version = '0.9'
    matrix_class = MatrixHandler

    @classmethod
    def test_perms(cls, mxid, role):
        perms = cls.config['bridge.permissions']
        for key in perms:
            if key == '*' and perms[key] == role:
                return True
            if key == mxid and perms[key] == role:
                return True
            user_domain = mxid.split(':')[1]
            if key == user_domain and perms[key] == role:
                return True

            return False

    def init_tables(self):
        for table in mautrix.bridge.db.UserProfile, mautrix.bridge.db.RoomState, db_tables.Users, db_tables.Rooms:
            table.bind(self.db)
        bdb.Base.metadata.create_all(self.db)

    def prepare_db(self) -> None:
        super().prepare_db()
        self.init_tables()


    def prepare_bridge(self):
        super().prepare_bridge()
        LmtpPortal.az = self.az
        LmtpPuppet.az = self.az # TODO?
        LmtpPortal.log = LmtpHandler.log = self.log
        LmtpPortal.config = LmtpPuppet.config = LmtpUser.config = LmtpBridge.config = self.config
        self.server = self.az.loop.run_until_complete(
            self.az.loop.create_server(
                factory, host=self.config["lmtp.bind"], port=self.config["lmtp.port"]))


def main():
    lmtp_bridge = LmtpBridge()
    lmtp_bridge.run()

main()
